@extends('layouts.admin')


{{-- Code CSS supplémentaire (soit des <link></link> soit <style></style>) --}}
@section('css')
<style>
    .file {
        visibility: hidden;
        position: absolute;
    }
</style>
@endsection

{{-- Code HTML --}}
@section('content')
<h4>Modification du profil #{{$star->id}} : {{$star->prenom}} {{$star->nom}}</h4>
<br>
<div class="card">
    <div class="card-body">
        <form action="{{route("stars.update", $star)}}" class="row" enctype="multipart/form-data" method="post">
            @csrf
            @method("PUT")
            <div class="col-sm-3">
                <div class="form-group">
                    <img src="{{($star->image) ? (strpos($star->image, "https") !== false ? $star->image : asset('storage/'.$star->image)) : asset("images/280x280.png")}}" style="width: 100%;" id="preview" class="img-thumbnail">
                </div>
                
                <div class="form-group">
                    <input type="file" name="image" class="file">
                    <div class="input-group my-3">
                        <input type="text" class="form-control custom-file-input" disabled
                            placeholder="Image de la star" id="file">
                        <div class="input-group-append">
                            <button type="button" class="browse btn btn-primary">Changer...</button>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-sm-9">
                <div class="mT-30">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6 form-col">
                                <label for="prenom">Prénom </label>
                            <input type="text" name="prenom" class="form-control" value="{{old("prenom") ? old("prenom") : $star->prenom}}" id="prenom" placeholder="Prénom'">
                            </div>
                            <div class="col-sm-6 form-col">
                                <label for="nom">Nom*</label>
                                <input type="text" name="nom" class="form-control"value="{{old("nom") ? old("nom") : $star->nom}}" id="nom" placeholder="Nom" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 form-col">
                                <label for="datetime">Date de naissance</label>
                                <input type="date" name="naissance" class="form-control" id="naissance" value="{{ isset($star->naissance) ? (new \DateTime($star->naissance))->format('Y-m-d'): ''}}">
                            </div>
                        </div>
                    </div>
                  
                    <div class="form-group">
                        <label for="description">Description </label>
                        <textarea name="description" cols="5" rows="5" class="form-control" id="description"
                            placeholder="Description de ">{{old("description") ? old("description") : $star->description}}</textarea>
                    </div>
                    <span class="small">(*) : Champs obligatoires</span>
                    <div class="text-right">
                        <a href="{{route("stars.index")}}" class="btn btn-secondary">Annuler</a>
                        <button type="submit" class="btn btn-success">Enregistrer</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

{{-- Code Scripts avec des balises <script></script> à ajouter--}}
@section('scripts')
<script>
    $(document).on("click", "#preview", function() {
    var file = $(this)
    .parent()
    .parent()
    .parent()
    .find(".file");
    file.trigger("click");
    });

    $(document).on("click", ".browse", function() {
    var file = $(this)
    .parent()
    .parent()
    .parent()
    .find(".file");
    file.trigger("click");
    });

    $('input[type="file"]').change(function(e) {
        var fileName = e.target.files[0].name;
        $("#file").val(fileName);
        
        var reader = new FileReader();
        reader.onload = function(e) {
            document.getElementById("preview").src = e.target.result;
        };
        reader.readAsDataURL(this.files[0]);
    });

</script>
@endsection