@extends("layouts.admin")

{{-- Code CSS supplémentaire (soit des <link></link> soit <style></style>) --}}
@section('css')
<link href="{{asset("datatables.net-bs4/css/dataTables.bootstrap4.css")}}" rel="stylesheet">
@endsection

@section("content")
<div class="card">
    <div class="card-body">
        <div class="d-flex align-items-center mb-4">
            <h4 class="card-title">Profils enregistrés</h4>
            <div class="ml-auto">
                <a href="{{route("stars.create")}}" class="btn btn-primary btn-sm"> <span class="d-none d-sm-inline"> Ajouter un profil</span> <span class="d-inline d-sm-none"> <i class="ti-plus"></i> </span></a>
                @if($stars->count()>0)
                <a href="#!"
                    onclick="deleteAll()"
                    class="btn btn-danger btn-sm"><span class="d-none d-sm-inline">Supprimer la sélection</span><span class="d-inline d-sm-none"><i class="ti-trash"></i></span></a>
                @endif
            </div>
        </div>
        <hr>
        @if($stars->count()>0)
        <div class="table-responsive">
            <form action="{{route("stars.multipleSuppression")}}" method="post" name="deleteForm">
                @csrf
                <table id="dataTable" class="table table-striped table-bordered display no-wrap" cellspacing="0"
                    style="width: 100%">

                    <thead>
                        <tr>
                            <th>
                                <label class="custom-control custom-checkbox be-select-all">
                                    <input class="custom-control-input chk_all" type="checkbox" name="chk_all"><span
                                        class="custom-control-label"></span>
                                </label>
                            </th>
                            <th>Star </th>
                            <th>Dernière modif. </th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>
                              
                            </th>
                            <th>Star </th>
                            <th>Dernière modif. </th>
                            <th>Actions</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @foreach ($stars->reverse() as $star)
                        <tr>
                            <td class="px-2 py-4">
                                <label class="custom-control custom-checkbox">
                                    <input class="custom-control-input checkboxes" type="checkbox" value="{{$star->id}}"
                                        name="ids[]" id="check{{$star->id}}"><span class="custom-control-label"></span>
                                </label>
                            </td>
                            <td class="px-2 py-4">
                                <div class="d-flex no-block align-items-center">
                                    <div class="mr-3"><img
                                            src="@if($star->image == null) {{asset("images/280x280.png")}} @else {{(strpos($star->image, "https")=== false) ? asset('storage/'.$star->image) : $star->image}} @endif"
                                            alt="Photo de la star" class="rounded-circle" width="45" height="45" />
                                    </div>
                                    <div class="">
                                        <h5 class="text-dark mb-0 font-16 font-weight-medium">
                                            {{$star->prenom}} {{strtoupper($star->nom)}}
                                        </h5>
                                        <span class="text-muted font-14 text-truncate">Né(e) le {{$star->naissance->format("d/m/Y")}}</span>
                                    </div>
                                </div>
                            </td>


                            <td class="text-left">
                                {{$star->updated_at->diffForHumans()}}
                            </td>
                            <td class="text-left">
                                <div class="btn-group" role="group" aria-label="First group">
                                    </button>
                                    <a type="button" title="Afficher la page de la star" class="btn btn-success"
                                        href={{ route("stars.show", $star) }}><i class="ti-eye"></i></a>
                                    </a>
                                    <a type="button" title="Modifier la star" class="btn btn-warning"
                                        href={{ route("stars.edit", $star) }}><i class="ti-pencil "></i></a>
                                    </a>
                                    <button type="button" title="Supprimer la star"
                                        onclick="deleteProfile({{$star->id}})"
                                        class="btn btn-danger">
                                        <span class=""><i class="ti-trash"></i></span>
                                    </button>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </form>
        </div>
        @else
        <div class="text-center">
            <h4 class="text-center">
                Aucune star n'a encore été ajoutée
            </h4>
            <img src="{{asset("images/empty.png")}}" class="img-fluid w-50" alt="Aucune star n'a encore été ajoutée"
                title="Aucune star n'a encore été ajoutée">
        </div>
        @endif
    </div>
</div>
@endsection

{{-- Code Scripts avec des balises <script></script> à ajouter--}}
@section('scripts')
<script src="{{asset("js/jquery.min.js")}}"></script>
<script src="{{asset("js/datatables/js/jquery.dataTables.min.js")}}"></script>
<script>
    $('#dataTable').DataTable({
        'paging' : true,
        'lengthChange': true,
        'searching' : true,
        'ordering' : true,
        'info' : true,
        'autoWidth' : true,
        fixedHeader: true,
        "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/French.json"
        }
    })
    $(".checkboxes").click(function() {
        
        if ($(".checkboxes").length == $(".subscheked:checked").length) {
            $(".chk_all").attr("checked", "checked");
        } else {
            $(".chk_all").removeAttr("checked");
        }
        
    });

    $(".chk_all").click(function() {
        var checkAll = $(".chk_all").prop('checked');
        if (checkAll) {
            $(".checkboxes").prop("checked", true);
        } else {
            $(".checkboxes").prop("checked", false);
        }
        
    });

    function deleteProfile(id){
        if(confirm('Êtes-vous sûr de vouloir supprimer cette(s) star(s) ?')){
            document.querySelector('#check'+id).setAttribute('checked', 'checked');
            document.deleteForm.submit();
        }
    }

    function deleteAll(){
        if(confirm('Êtes-vous sûr de vouloir supprimer cette(s) star(s) ?')){
            document.deleteForm.submit();
        } 
    }
</script>
@endsection