<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Profile Browser</title>
    <!-- CSS only -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <!-- JS, Popper.js, and jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous">
    </script>

    <style>
        .profile {
            width: 10rem;
            float: left;
            margin-right: 1rem;
            margin-bottom: 1rem;
        }

        .border-radius {
            border-radius: 0;
        }
    </style>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <div class="container">
            <a class="navbar-brand" href="{{route("home")}}">Profile Browser</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
                aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarText">
                @guest
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{route("register")}}">Inscription</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="{{route("login")}}">Connexion</a>
                    </li>
                </ul>
                @endguest

                @auth
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{route("stars.index")}}">Administration</a>
                    </li>
                </ul>
                @endauth
            </div>
        </div>
    </nav>
    <section>
        <div class="container">
            <div class="row mt-5">
                <div class="col-sm-3 p-0">
                    @foreach($stars as $s)
                    <a href="{{route("home", ["star"=>$s, "page"=>Request::get('page')])}}"
                        class="w-100 btn btn-sm border text-truncate @if($star->id == $s->id) btn-primary border-primary border-radius @else btn-light @endif">{{ucfirst($s->prenom)}}
                        {{ucfirst($s->nom)}}</a>
                    @if($star->id == $s->id)
                    <div class="d-sm-none border border-primary p-2 mt-2 mb-2">
                        <img src="@if($star->image == null) {{asset("images/280x280.png")}}@else {{(strpos($star->image, "https")=== false) ? asset('storage/'.$star->image) : $star->image}}@endif"
                            alt="Image de {{ucfirst($star->prenom)}} {{ucfirst($star->nom)}}"
                            class="img-fluid profile img-thumbnail">
                        <h3>{{ucfirst($star->prenom)}} {{ucfirst($star->nom)}}</h3>
                        <small>Naissance : {{$star->naissance->format("d/m/Y")}}
                            ({{$star->naissance->diffForHumans()}})</small>
                        <hr>
                        {{$star->description}}
                    </div>
                    @endif
                    @endforeach
                </div>
                <div class="col-sm-9 bg-light border-primary border p-4 d-sm-block d-none">
                    @if($star->id)
                    <div>
                        <img src="@if($star->image == null) {{asset("images/280x280.png")}} @else {{(strpos($star->image, "https")=== false) ? asset('storage/'.$star->image) : $star->image}} @endif"
                            alt="Image de {{ucfirst($star->prenom)}} {{ucfirst($star->nom)}}"
                            class="img-fluid profile img-thumbnail">
                        <h3>{{ucfirst($star->prenom)}} {{ucfirst($star->nom)}}</h3>
                        <small>Naissance : {{$star->naissance->format("d/m/Y")}}
                            ({{$star->naissance->diffForHumans()}})</small>
                        <hr>
                        {{$star->description}}
                    </div>
                    @else

                    <div>
                        <h3>Profile Browser</h3>
                        <h5>Introduction</h5>
                        <hr>
                        <p>
                            Bienvenue sur Profile Browser.
                            <br>
                            Retrouvez des informations sur des dizaines de profils différents. Cliquez sur le nom d'un
                            profil dans la colonne à gauche pour avoir les détails.
                            <br>
                            Si vous le souhaitez, vous avez la possibilité de modifier les informations d'un profil à
                            savoir :
                            <ul>
                                <li>Sa photo</li>
                                <li>Son nom</li>
                                <li>Son prenom</li>
                                <li>Sa date de naissance</li>
                                <li>Sa description</li>
                            </ul>
                            Pour le faire, connectez-vous. Les identifiants de connexion sont :
                            <ul>
                                <li>Email : admin@admin.com</li>
                                <li>Mot de passe : password</li>
                            </ul>
                        </p>
                    </div>
                    @endif
                </div>
        </div>
        {{$stars->links()}}
        </div>
    </section>
    <footer class="p-3 pt-5 text-center">
        Copyright &#169; 2015 - {{date("Y")}} <a target="_blank" href="https://hellocse.fr">HelloCSE</a>.
    </footer>
</body>

</html>