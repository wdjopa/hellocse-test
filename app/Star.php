<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Star extends Model
{
    //
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'prenom', 'nom', 'image', 'description', 'naissance',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'naissance' => 'datetime',
    ];

    public static $rules = [
        'nom' => 'required',
    ];
}
