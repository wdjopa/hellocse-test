<?php

namespace App\Http\Controllers;

use App\Star;
use Illuminate\Http\Request;

use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class StarController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stars = Star::all();
        return view("stars.index", compact("stars"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view("stars.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validation des règles 
        $rules = Star::$rules;
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()->with("error", "Le champ nom est obligatoire.".json_encode($validator->failed()));
        }

        // Création du profil
        $star = Star::create($request->all());

        // Mise à jour de l'image du profil 
        if($request->file("image")){
            $path = $request->file("image")->store("images", "public");
            $star->image = $path;
        }
        $star->save();

        return redirect()->route("stars.index")->with("success", "Le profil a bien été créé.");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Star  $star
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Star $star)
    {
        $stars = Star::paginate(100);
        return view("welcome", compact("stars", "star"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Star  $star
     * @return \Illuminate\Http\Response
     */
    public function edit(Star $star)
    {
        return view("stars.edit", compact("star"));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Star  $star
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Star $star)
    {
        // 
        $rules = Star::$rules;
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()->with("error", "Le champ nom est obligatoire.".json_encode($validator->failed()));
        }

        if($request->file("image")){
            $path = $request->file("image")->store("images", "public");
            $star->image = $path;
        }
        $star->nom = $request->nom;
        $star->prenom = $request->prenom;
        $star->description = $request->description;
        $star->naissance = $request->naissance;
        $star->save();
        return redirect()->route("stars.index")->with("success", "Le profil a bien été modifié.");
    }

    /**
     * Supprime une star précise.
     *
     * @param  \App\Star  $star
     * @return \Illuminate\Http\Response
     */
    public function destroy(Star $star)
    {
        $star->delete();
        return redirect()->route("stars.index")->with('success', "Le profil a été supprimé avec succès");
    }

     /**
     * Supprime une liste de profils à partir de leur ID.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function multipleSuppression(Request $request)
    {
        if($request->ids){
            $ids = $request->ids;
            foreach ($ids as $id) {
                $star = Star::find($id);
                $star->delete();
            }
            $message = sizeof($ids).' profil(s) supprimé(s) avec succès';
            $type = "success";
        }
        else{
            $type = "warning";
            $message = "Aucun profil n'a été supprimé";
        }
        return redirect()->route("stars.index")->with($type, $message);
    }
}
