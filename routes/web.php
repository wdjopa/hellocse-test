<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();


Route::get("stars/{star}", "StarController@show")->name("stars.show");
Route::get('/{star?}',"StarController@show")->name("home")->where('star', '[0-9]+');


Route::group(["middleware"=>["auth"], "prefix"=>"dashboard"], function (){
    Route::resource("stars", "StarController")->except("show");
    Route::post("stars/suppressions", "StarController@multipleSuppression")->name("stars.multipleSuppression");
});
