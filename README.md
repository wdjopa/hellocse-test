<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## À propos

Dans le cadre d'un test de développement Web avec Laravel. J'ai réalisé une mini application avec Backoffice et page publique permettant d'afficher des informations sur des fiches.

## Demo

Pour avoir une demo du résultat final, vous pouvez vous rendre sur https://hellocse.djopa.fr/


## Installation

Après avoir cloné le projet, vous devrez lancer la commande suivante à la racine du projet. 
```sh
composer install
``` 

Vous devrez renommer le fichier .env.example en .env et **modifier le nom de la base de données** en remplacant par un nom que vous le souhaitez si vous le voulez. Puis, vous devrez créer cette base de donnée.

Une fois la base de donnée créée, vous devrez exécuter les commandes suivantes :

```php
    php artisan key:generate
```
```php
    php artisan storage:link
```

```php
    php artisan migrate --seed
```

Vous pourrez ensuite lancer le projet avec la commande

```php
    php artisan serve
```

### Note

Un compte par défaut est créé. Les identifiants sont :
> Email : admin@admin.com
>
> Mot de passe : password

Des informations sur des profils sont générés et ajoutés à la base de données. Vous avez la possibilité depuis l'outil de backoffice de gérer toutes ces informations.