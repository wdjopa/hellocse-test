<?php

use Faker\Factory;
use Illuminate\Database\Seeder;

class StarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'name' => "Admin",
            'email' => "admin@admin.com",
            'password' => Hash::make('password'),
        ]);
        
        for($i=0;$i<20;$i++){
            $faker = Factory::create();
            DB::table('stars')->insert(
                [
                    'prenom'=>$faker->firstName,
                    'nom'=>$faker->lastName,
                    // 'prenom'=>ucfirst(Str::random(7)),
                    // 'nom'=>strtoupper(Str::random(5)),
                    'image'=>"https://picsum.photos/300/300?random=1",
                    'description'=>$faker->text(1200),
                    'naissance'=> date("Y-m-d H:i:s",mt_rand(-631155600,946681200)),
                    "created_at"=>new \DateTime(),
                    "updated_at"=>new \DateTime()
                ],
            );
        }

    }
}
